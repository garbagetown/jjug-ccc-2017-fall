CI-CD
=====

## AMI 
bitnami の AMI はなぜか t1.micro 限定で重くて使い物にならなかった...

## 自前インストール
t2.micro でチャレンジ。

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [9:02:43] 
$ ssh -i ~/Desktop/ci-cd.pem ec2-user@52.199.104.164
The authenticity of host '52.199.104.164 (52.199.104.164)' can't be established.
ECDSA key fingerprint is SHA256:pB/NCGVgkiga3wAv4INHWuOtv8DeCKangsvKhbDLoYc.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '52.199.104.164' (ECDSA) to the list of known hosts.

       __|  __|_  )
       _|  (     /   Amazon Linux AMI
      ___|\___|___|

https://aws.amazon.com/amazon-linux-ami/2017.09-release-notes/
1 package(s) needed for security, out of 6 available
Run "sudo yum update" to apply all updates.
```

Java 1.7 が入っているので、このまま進める。
```sh
[ec2-user@ip-172-31-18-103 ~]$ java -version
java version "1.7.0_151"
OpenJDK Runtime Environment (amzn-2.6.11.0.74.amzn1-x86_64 u151-b00)
OpenJDK 64-Bit Server VM (build 24.151-b00, mixed mode)
```

war ファイルをダウンロード
```sh
[ec2-user@ip-172-31-18-103 ~]$ wget http://mirrors.jenkins.io/war-stable/latest/jenkins.war
--2017-10-31 00:29:54--  http://mirrors.jenkins.io/war-stable/latest/jenkins.war
Resolving mirrors.jenkins.io (mirrors.jenkins.io)... 52.202.51.185
Connecting to mirrors.jenkins.io (mirrors.jenkins.io)|52.202.51.185|:80... connected.
HTTP request sent, awaiting response... 302 Found
Location: http://ftp-nyc.osuosl.org/pub/jenkins/war-stable/2.73.2/jenkins.war [following]
--2017-10-31 00:29:54--  http://ftp-nyc.osuosl.org/pub/jenkins/war-stable/2.73.2/jenkins.war
Resolving ftp-nyc.osuosl.org (ftp-nyc.osuosl.org)... 64.50.233.100, 2600:3404:200:237::2
Connecting to ftp-nyc.osuosl.org (ftp-nyc.osuosl.org)|64.50.233.100|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 73280014 (70M) [application/x-java-archive]
Saving to: ‘jenkins.war’

jenkins.war                                  100%[============================================================================================>]  69.88M  6.87MB/s    in 21s     

2017-10-31 00:30:16 (3.39 MB/s) - ‘jenkins.war’ saved [73280014/73280014]
```

起動。あれー
```sh
[ec2-user@ip-172-31-18-103 ~]$ java -jar jenkins.war 
Jenkins requires Java8 or later, but you are running 1.7.0_151-mockbuild_2017_08_09_21_42-b00 from /usr/lib/jvm/java-1.7.0-openjdk-1.7.0.151.x86_64/jre
java.lang.UnsupportedClassVersionError: 51.0
	at Main.main(Main.java:124)
```

ダウンロードして転送
```sh
# garbagetown @ garbagetown in ~/Desktop [9:43:02] 
$ scp -i ~/Desktop/ci-cd.pem jdk-8u151-linux-x64.rpm ec2-user@52.199.104.164:~/              
jdk-8u151-linux-x64.rpm                                                                                                                         100%  166MB   2.5MB/s   01:06    
```

インストール
```sh
[ec2-user@ip-172-31-18-103 ~]$ ll
total 241648
-rw-r--r-- 1 ec2-user ec2-user 174163338 Oct 31 00:44 jdk-8u151-linux-x64.rpm
-rw-rw-r-- 1 ec2-user ec2-user  73280014 Oct  9 19:22 jenkins.war
[ec2-user@ip-172-31-18-103 ~]$ sudo rpm -ivh jdk-8u151-linux-x64.rpm 
Preparing...                          ################################# [100%]
Updating / installing...
   1:jdk1.8-2000:1.8.0_151-fcs        ################################# [100%]
Unpacking JAR files...
	tools.jar...
	plugin.jar...
	javaws.jar...
	deploy.jar...
	rt.jar...
	jsse.jar...
	charsets.jar...
	localedata.jar...

[ec2-user@ip-172-31-18-103 ~]$ java -version
java version "1.8.0_151"
Java(TM) SE Runtime Environment (build 1.8.0_151-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.151-b12, mixed mode)
```

再度起動
```sh
[ec2-user@ip-172-31-18-103 ~]$ java -jar jenkins.war 
Running from: /home/ec2-user/jenkins.war
webroot: $user.home/.jenkins
(snip)
Oct 31, 2017 12:47:37 AM jenkins.install.SetupWizard init
INFO: 

*************************************************************
*************************************************************
*************************************************************

Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:

bd8f6352a8394fa4bad204517ee8f05c

This may also be found at: /home/ec2-user/.jenkins/secrets/initialAdminPassword

*************************************************************
*************************************************************
*************************************************************
(snip)
INFO: Obtained the updated data file for hudson.tools.JDKInstaller
Oct 31, 2017 12:47:44 AM hudson.model.AsyncPeriodicWork$1 run
INFO: Finished Download metadata. 9,811 ms
```

52.199.104.164:8080 にアクセスして上記パスワードを入力。
suggested plugins をインストール。
admin/admin で admin ユーザを作成。

なんとなく GitLab Plugin をインストール


なんか git clone できないと思ったら git が入ってなかった・・・
```sh
[ec2-user@ip-172-31-18-103 ~]$ sudo yum -y install git
Loaded plugins: priorities, update-motd, upgrade-helper
(snip)
Installed:
  git.x86_64 0:2.13.6-1.55.amzn1                                                                                                                                                  

Dependency Installed:
  perl-Error.noarch 1:0.17020-2.9.amzn1                     perl-Git.noarch 0:2.13.6-1.55.amzn1                     perl-TermReadKey.x86_64 0:2.30-20.9.amzn1                    

Complete!

[ec2-user@ip-172-31-18-103 ~]$ git --version
git version 2.13.6
```

## Jenkins 再インストール

yum で入るらしいのでそっちのが楽。

デフォルトリポジトリには見つからない。
```sh
[ec2-user@ip-172-31-18-103 ~]$ sudo yum info jenkins
Loaded plugins: priorities, update-motd, upgrade-helper
Error: No matching Packages to list
```

リポジトリ追加してインストール。
```sh
[ec2-user@ip-172-31-18-103 ~]$ sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo
(snip)
2017-10-31 06:12:31 (17.7 MB/s) - ‘/etc/yum.repos.d/jenkins.repo’ saved [85/85]

[ec2-user@ip-172-31-18-103 ~]$ sudo yum info jenkins
Loaded plugins: priorities, update-motd, upgrade-helper
(snip)
Available Packages
Name        : jenkins
Arch        : noarch
Version     : 2.73.2
Release     : 1.1
Size        : 70 M
Repo        : jenkins
Summary     : Jenkins Automation Server
URL         : http://jenkins.io/
License     : MIT/X License, GPL/CDDL, ASL2
Description : Jenkins is an open source automation server which enables developers around the world to reliably automate  their development lifecycle processes of all kinds,
            : including build, document, test, package, stage, deployment, static analysis and many more.
            : 
            : Jenkins is being widely used in areas of Continuos Integration, Continuous Delivery, DevOps, and other areas. And it is not only about software, the same automation
            : techniques can be applied in other areas like Hardware Engineering, Embedded Systems, BioTech, etc.
            : 
            : For information see https://jenkins.io
            : 
            : 
            : Authors:
            : --------
            :     Kohsuke Kawaguchi <kk@kohsuke.org>


[ec2-user@ip-172-31-18-103 ~]$ sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key

[ec2-user@ip-172-31-18-103 ~]$ sudo yum -y install jenkins
(snip)
Installed:
  jenkins.noarch 0:2.73.2-1.1                                                                                                                                                     

Complete!
```

起動
```sh
[ec2-user@ip-172-31-18-103 ~]$ sudo chkconfig --list | grep jenkins
jenkins        	0:off	1:off	2:off	3:on	4:off	5:on	6:off

[ec2-user@ip-172-31-18-103 ~]$ sudo service jenkins start
Starting Jenkins                                           [  OK  ]

[ec2-user@ip-172-31-18-103 ~]$ sudo chkconfig jenkins on

[ec2-user@ip-172-31-18-103 ~]$ sudo chkconfig --list | grep jenkins
jenkins        	0:off	1:off	2:on	3:on	4:on	5:on	6:off
```

初回パスワード
```
[ec2-user@ip-172-31-18-103 ~]$ sudo cat /var/lib/jenkins/secrets/initialAdminPassword
14683933c1ba4a849390af2a137935a5
```

- [CentOS7.0にJenkinsをインストールする - Qiita](https://qiita.com/inakadegaebal/items/b526ffbdbe7ff2b443f1)



## nginx で proxy

したい。
EIP を挿して www.grgb.work を route53 で登録。

yum で最新の安定版が入るっぽい
```sh
[ec2-user@ip-172-31-18-103 ~]$ sudo yum info nginx
Loaded plugins: priorities, update-motd, upgrade-helper
Available Packages
Name        : nginx
Arch        : x86_64
Epoch       : 1
Version     : 1.12.1
Release     : 1.33.amzn1
Size        : 561 k
Repo        : amzn-main/latest
Summary     : A high performance web server and reverse proxy server
URL         : http://nginx.org/
License     : BSD
Description : Nginx is a web server and a reverse proxy server for HTTP, SMTP, POP3 and
            : IMAP protocols, with a strong focus on high concurrency, performance and low
            : memory usage.
```

```sh
[ec2-user@ip-172-31-18-103 ~]$ sudo yum -y install nginx
Loaded plugins: priorities, update-motd, upgrade-helper
(snip)
Installed:
  nginx.x86_64 1:1.12.1-1.33.amzn1                                                                                                                                                

Dependency Installed:
  gperftools-libs.x86_64 0:2.0-11.5.amzn1                                                    libunwind.x86_64 0:1.1-10.8.amzn1                                                   

Complete!

[ec2-user@ip-172-31-18-103 ~]$ nginx -v
nginx version: nginx/1.12.1
```

起動
```sh
[ec2-user@ip-172-31-18-103 ~]$ sudo chkconfig --list | grep nginx
nginx          	0:off	1:off	2:off	3:off	4:off	5:off	6:off

[ec2-user@ip-172-31-18-103 ~]$ sudo service nginx start
Starting nginx:                                            [  OK  ]

[ec2-user@ip-172-31-18-103 ~]$ sudo chkconfig nginx on

[ec2-user@ip-172-31-18-103 ~]$ sudo chkconfig --list | grep nginx
nginx          	0:off	1:off	2:on	3:on	4:on	5:on	6:off
```

見様見真似で proxy 設定
```sh
[ec2-user@ip-172-31-18-103 ~]$ sudo vi /etc/nginx/conf.d/jenkins.conf
[ec2-user@ip-172-31-18-103 ~]$ sudo cat /etc/nginx/conf.d/jenkins.conf 
server {

  server_name jenkins.grgb.work;

  location / {
    proxy_pass http://127.0.0.1:8080/;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Host $host:$server_port;
    proxy_set_header X-Forwarded-Server $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  }
}
```

nginx を再起動
```sh
[ec2-user@ip-172-31-18-103 ~]$ sudo service nginx restart
Stopping nginx:                                            [  OK  ]
Starting nginx:                                            [  OK  ]
```


## gradle 

gradle も入ってない・・・

- [Cannot run program "gradle" in Jenkins - Stack Overflow](https://stackoverflow.com/questions/8646762/cannot-run-program-gradle-in-jenkins/23108867#23108867)

以下、手動でインストールしたのは間違い
```sh
[ec2-user@ip-172-31-18-103 ~]$ wget https://services.gradle.org/distributions/gradle-4.3-bin.zip
(snip)
2017-10-31 07:38:35 (72.8 MB/s) - ‘gradle-4.3-bin.zip’ saved [73018108/73018108]

[ec2-user@ip-172-31-18-103 ~]$ sudo mkdir /opt/gradle

[ec2-user@ip-172-31-18-103 ~]$ sudo unzip -d /opt/gradle gradle-4.3-bin.zip 
Archive:  gradle-4.3-bin.zip
   creating: /opt/gradle/gradle-4.3/
(snip)
  inflating: /opt/gradle/gradle-4.3/lib/plugins/hamcrest-core-1.3.jar  

[ec2-user@ip-172-31-18-103 ~]$ vi ~/.bash_profile 
[ec2-user@ip-172-31-18-103 ~]$ grep gradle ~/.bash_profile 
PATH=$PATH:/opt/gradle/gradle-4.3/bin/
[ec2-user@ip-172-31-18-103 ~]$ source ~/.bash_profile 
[ec2-user@ip-172-31-18-103 ~]$ gradle -v

------------------------------------------------------------
Gradle 4.3
------------------------------------------------------------

Build time:   2017-10-30 15:43:29 UTC
Revision:     c684c202534c4138b51033b52d871939b8d38d72

Groovy:       2.4.12
Ant:          Apache Ant(TM) version 1.9.6 compiled on June 29 2015
JVM:          1.8.0_151 (Oracle Corporation 25.151-b12)
OS:           Linux 4.9.51-10.52.amzn1.x86_64 amd64
```

- [How to Install Gradle on CentOS 7 - Vultr.com](https://www.vultr.com/docs/how-to-install-gradle-on-centos-7)

jenkins ユーザの PATH に通ってない
```sh
[ec2-user@ip-172-31-18-103 ~]$ sudo su -s /bin/bash - jenkins
Last login: Tue Oct 31 15:20:51 UTC 2017 on pts/0
-bash-4.2$ gradle
-bash: gradle: command not found
-bash-4.2$ env | grep PATH
PATH=/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/opt/aws/bin
AWS_PATH=/opt/aws
```

## GraphViz 

も入ってない。

```sh
[ec2-user@ip-172-31-18-103 ~]$ sudo wget -O /etc/yum.repos.d/graphviz-rhel.repo http://www.graphviz.org/graphviz-rhel.repo
(snip)
2017-10-31 15:47:56 (292 MB/s) - ‘/etc/yum.repos.d/graphviz-rhel.repo’ saved [1138/1138]

[ec2-user@ip-172-31-18-103 ~]$ sudo yum list available 'graphviz*'
Loaded plugins: priorities, update-motd, upgrade-helper
amzn-main                                                                                                                                                  | 2.1 kB  00:00:00     
amzn-updates                                                                                                                                               | 2.5 kB  00:00:00     
http://www.graphviz.org/pub/graphviz/stable/redhat/ellatest/x86_64/os/repodata/repomd.xml: [Errno 14] HTTP Error 404 - Not Found
Trying other mirror.
To address this issue please refer to the below knowledge base article 

https://access.redhat.com/articles/1320623

If above article doesn't help to resolve this issue please open a ticket with Red Hat Support.

jenkins                                                                                                                                                    | 2.9 kB  00:00:00     
Available Packages
graphviz.i686                                                                          2.38.0-18.50.amzn1                                                                amzn-main
graphviz.x86_64                                                                        2.38.0-18.50.amzn1                                                                amzn-main
graphviz-R.x86_64                                                                      2.38.0-18.50.amzn1                                                                amzn-main
graphviz-devel.x86_64                                                                  2.38.0-18.50.amzn1                                                                amzn-main
graphviz-doc.x86_64                                                                    2.38.0-18.50.amzn1                                                                amzn-main
graphviz-gd.x86_64                                                                     2.38.0-18.50.amzn1                                                                amzn-main
graphviz-graphs.x86_64                                                                 2.38.0-18.50.amzn1                                                                amzn-main
graphviz-guile.x86_64                                                                  2.38.0-18.50.amzn1                                                                amzn-main
graphviz-java.x86_64                                                                   2.38.0-18.50.amzn1                                                                amzn-main
graphviz-lua.x86_64                                                                    2.38.0-18.50.amzn1                                                                amzn-main
graphviz-perl.x86_64                                                                   2.38.0-18.50.amzn1                                                                amzn-main
graphviz-php.x86_64                                                                    2.38.0-18.43.amzn1                                                                amzn-main
graphviz-php54.x86_64                                                                  2.38.0-18.50.amzn1                                                                amzn-main
graphviz-python26.x86_64                                                               2.38.0-18.50.amzn1                                                                amzn-main
graphviz-python27.x86_64                                                               2.38.0-18.50.amzn1                                                                amzn-main
graphviz-ruby.x86_64                                                                   2.38.0-18.50.amzn1                                                                amzn-main
graphviz-tcl.x86_64                                                                    2.38.0-18.50.amzn1                                                                amzn-main

[ec2-user@ip-172-31-18-103 ~]$ sudo yum -y --skip-broken install 'graphviz*'
(snip)

Skipped (dependency problems):
  graphviz-php.x86_64 0:2.38.0-18.43.amzn1    graphviz-php54.x86_64 0:2.38.0-18.50.amzn1    libzip.x86_64 0:0.10.1-1.3.amzn1            php-common.x86_64 0:5.3.29-1.8.amzn1   
  php54-cli.x86_64 0:5.4.45-1.75.amzn1        php54-common.x86_64 0:5.4.45-1.75.amzn1       php54-process.x86_64 0:5.4.45-1.75.amzn1    php54-xml.x86_64 0:5.4.45-1.75.amzn1   

Complete!

[ec2-user@ip-172-31-18-103 ~]$ dot -V
dot - graphviz version 2.38.0 (20140413.2041)
```

- [rhel | Graphviz - Graph Visualization Software](http://www.graphviz.org/Download_linux_rhel.php)

日本語が文字化けしてる