memo
=====

## gradle 

入ってなかった・・・
```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [5:52:06] 
$ gradle
zsh: command not found: gradle
```

ものすごく古い gradle しかない。
```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [5:52:10] C:127
$ brew -v
Homebrew >1.2.0 (no git repository)
Homebrew/homebrew-core (git revision 078d; last commit 2017-07-30)

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [5:54:10] 
$ brew search gradle
gradle                                                     gradle-completion                                          gradle@2.14
```

SDKMAN! でインストールするものらしい。
```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [5:55:07] 
$ curl -s http://get.sdkman.io/ | bash
                                                               		         
Thanks for using...                                            		         
                                                               		         
                                                               		         
     SSSSSSSSSSSSSSS DDDDDDDDDDDDD       KKKKKKKKK    KKKKKKK                  
   SS:::::::::::::::SD::::::::::::DDD    K:::::::K    K:::::K                  
  S:::::SSSSSS::::::SD:::::::::::::::DD  K:::::::K    K:::::K                  
  S:::::S     SSSSSSSDDD:::::DDDDD:::::D K:::::::K   K::::::K                  
  S:::::S              D:::::D    D:::::DKK::::::K  K:::::KKK                  
  S:::::S              D:::::D     D:::::D K:::::K K:::::K                     
   S::::SSSS           D:::::D     D:::::D K::::::K:::::K                      
    SS::::::SSSSS      D:::::D     D:::::D K:::::::::::K                       
      SSS::::::::SS    D:::::D     D:::::D K:::::::::::K                       
         SSSSSS::::S   D:::::D     D:::::D K::::::K:::::K                      
              S:::::S  D:::::D     D:::::D K:::::K K:::::K                     
              S:::::S  D:::::D    D:::::DKK::::::K  K:::::KKK                  
  SSSSSSS     S:::::SDDD:::::DDDDD:::::D K:::::::K   K::::::K                  
  S::::::SSSSSS:::::SD:::::::::::::::DD  K:::::::K    K:::::K                  
  S:::::::::::::::SS D::::::::::::DDD    K:::::::K    K:::::K                  
   SSSSSSSSSSSSSSS   DDDDDDDDDDDDD       KKKKKKKKK    KKKKKKK                  
                                                                               
                                                                               
                      mmmmmmm    mmmmmmm     aaaaaaaaaaaaa  nnnn  nnnnnnnn     
                    mm:::::::m  m:::::::mm   a::::::::::::a n:::nn::::::::nn   
                   m::::::::::mm::::::::::m  aaaaaaaaa:::::an::::::::::::::nn  
                   m::::::::::::::::::::::m           a::::ann:::::::::::::::n 
                   m:::::mmm::::::mmm:::::m    aaaaaaa:::::a  n:::::nnnn:::::n 
                   m::::m   m::::m   m::::m  aa::::::::::::a  n::::n    n::::n 
                   m::::m   m::::m   m::::m a::::aaaa::::::a  n::::n    n::::n 
                   m::::m   m::::m   m::::ma::::a    a:::::a  n::::n    n::::n 
                   m::::m   m::::m   m::::ma::::a    a:::::a  n::::n    n::::n 
                   m::::m   m::::m   m::::ma:::::aaaa::::::a  n::::n    n::::n 
                   m::::m   m::::m   m::::m a::::::::::aa:::a n::::n    n::::n 
                   mmmmmm   mmmmmm   mmmmmm  aaaaaaaaaa  aaaa nnnnnn    nnnnnn 
            								                                     
            								                                     
                                                 Now attempting installation...
                                                                               
Looking for a previous installation of SDKMAN...
Looking for unzip...
Looking for zip...
Looking for curl...
Looking for sed...
Installing SDKMAN scripts...
Create distribution directories...
Getting available candidates...
Prime the config file...
Download script archive...
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
  0     0    0     0    0     0      0      0 --:--:--  0:00:01 --:--:--     0
100 20938  100 20938    0     0   7314      0  0:00:02  0:00:02 --:--:-- 60166
Extract script archive...
Install scripts...
Set version to 5.5.12+269 ...
Updated existing /Users/garbagetown/.bashrc
Attempt update of zsh profiles...
Updated existing /Users/garbagetown/.zshrc



All done!


Please open a new terminal, or run the following in the existing one:

    source "/Users/garbagetown/.sdkman/bin/sdkman-init.sh"

Then issue the following command:

    sdk help

Enjoy!!!

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [5:56:36] 
$ source "/Users/garbagetown/.sdkman/bin/sdkman-init.sh"

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [5:57:31] 
$ sdk help
==== BROADCAST =================================================================
* 15/10/17: Oracle JDK 9+181 and 8u144 available for Linux, MacOSX and Cygwin. #java
* 12/10/17: Gradle 4.3-rc-1 released on SDKMAN! #gradle
* 05/10/17: Groovy 2.5.0-beta-2 released on SDKMAN! #groovylang
================================================================================

Usage: sdk <command> [candidate] [version]
       sdk offline <enable|disable>

   commands:
       install   or i    <candidate> [version]
       uninstall or rm   <candidate> <version>
       list      or ls   [candidate]
       use       or u    <candidate> [version]
       default   or d    <candidate> [version]
       current   or c    [candidate]
       upgrade   or ug   [candidate]
       version   or v
       broadcast or b
       help      or h
       offline           [enable|disable]
       selfupdate        [force]
       flush             <candidates|broadcast|archives|temp>

   candidate  :  the SDK to install: groovy, scala, grails, gradle, kotlin, etc.
                 use list command for comprehensive list of candidates
                 eg: $ sdk list

   version    :  where optional, defaults to latest stable if not provided
                 eg: $ sdk install groovy

```

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [5:57:38] 
$ sdk list gradle

================================================================================
Available Gradle Versions
================================================================================
     4.3-rc-1             3.2                  2.12                 1.10           
     4.2.1                3.1                  2.11                 1.1            
     4.2-rc-2             3.0                  2.10                 1.0            
     4.2-rc-1             2.9                  2.1                  0.9.2          
     4.2                  2.8                  2.0                  0.9.1          
     4.1                  2.7                  1.9                  0.9            
     4.0.2                2.6                  1.8                  0.8            
     4.0.1                2.5                  1.7                  0.7            
     4.0                  2.4                  1.6                                 
     3.5.1                2.3                  1.5                                 
     3.5                  2.2.1                1.4                                 
     3.4.1                2.2                  1.3                                 
     3.4                  2.14.1               1.2                                 
     3.3                  2.14                 1.12                                
     3.2.1                2.13                 1.11                                

================================================================================
+ - local version
* - installed
> - currently in use
================================================================================

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [5:58:38] 
$ sdk install gradle 4.2.1

Downloading: gradle 4.2.1

In progress...

######################################################################## 100.0%

Installing: gradle 4.2.1
Done installing!


Setting gradle 4.2.1 as default.

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [5:59:40] 
$ gradle -v

------------------------------------------------------------
Gradle 4.2.1
------------------------------------------------------------

Build time:   2017-10-02 15:36:21 UTC
Revision:     a88ebd6be7840c2e59ae4782eb0f27fbe3405ddf

Groovy:       2.4.12
Ant:          Apache Ant(TM) version 1.9.6 compiled on June 29 2015
JVM:          1.8.0_131 (Oracle Corporation 25.131-b11)
OS:           Mac OS X 10.11.6 x86_64
```


## graphviz

graphviz も入ってない。
```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [6:08:51] 
$ dot -V
zsh: command not found: dot
```

`brew upgrade` してからインストール。
```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [6:28:14] C:1
$ brew install graphviz
==> Installing dependencies for graphviz: libtool, freetype, fontconfig, webp, gd
==> Installing graphviz dependency: libtool
==> Downloading https://homebrew.bintray.com/bottles/libtool-2.4.6_1.el_capitan.bottle.tar.gz
######################################################################## 100.0%
==> Pouring libtool-2.4.6_1.el_capitan.bottle.tar.gz
==> Caveats
In order to prevent conflicts with Apple's own libtool we have prepended a "g"
so, you have instead: glibtool and glibtoolize.
==> Summary
🍺  /usr/local/Cellar/libtool/2.4.6_1: 70 files, 3.7MB
==> Installing graphviz dependency: freetype
==> Downloading https://homebrew.bintray.com/bottles/freetype-2.8.1.el_capitan.bottle.tar.gz
######################################################################## 100.0%
==> Pouring freetype-2.8.1.el_capitan.bottle.tar.gz
🍺  /usr/local/Cellar/freetype/2.8.1: 63 files, 2.7MB
==> Installing graphviz dependency: fontconfig
==> Downloading https://homebrew.bintray.com/bottles/fontconfig-2.12.6.el_capitan.bottle.tar.gz
######################################################################## 100.0%
==> Pouring fontconfig-2.12.6.el_capitan.bottle.tar.gz
==> Regenerating font cache, this may take a while
==> /usr/local/Cellar/fontconfig/2.12.6/bin/fc-cache -frv

🍺  /usr/local/Cellar/fontconfig/2.12.6: 493 files, 3.2MB
==> Installing graphviz dependency: webp
==> Downloading https://homebrew.bintray.com/bottles/webp-0.6.0_2.el_capitan.bottle.tar.gz
######################################################################## 100.0%
==> Pouring webp-0.6.0_2.el_capitan.bottle.tar.gz
🍺  /usr/local/Cellar/webp/0.6.0_2: 36 files, 2.0MB
==> Installing graphviz dependency: gd
==> Downloading https://homebrew.bintray.com/bottles/gd-2.2.5.el_capitan.bottle.tar.gz
######################################################################## 100.0%
==> Pouring gd-2.2.5.el_capitan.bottle.tar.gz
🍺  /usr/local/Cellar/gd/2.2.5: 35 files, 1.1MB
==> Installing graphviz
==> Downloading https://homebrew.bintray.com/bottles/graphviz-2.40.1.el_capitan.bottle.1.tar.gz
######################################################################## 100.0%
==> Pouring graphviz-2.40.1.el_capitan.bottle.1.tar.gz
🍺  /usr/local/Cellar/graphviz/2.40.1: 536 files, 12.9MB

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [6:36:06]
$ dot -V
dot - graphviz version 2.40.1 (20161225.0304)
```

## asciidoctor 

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [6:05:05] 
$ tree src        
src
└── docs
    └── asciidoc
        └── index.adoc

2 directories, 1 file

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [6:38:36] 
$ cat src/docs/asciidoc/index.adoc
= サンプル設計書
サンプル太郎 <sample@example.com>
v1.0, 2016-03-15
:toc:

== PlantUMLによるクラス図の例
UMLはPlantUMLで記述できる。日本語（UTF-8）でも記述可能。

.クラス図
[plantuml, classediagram, png]
....
class "クラスA" {
  +メソッド1()
}
class "クラスB" {
  +メソッド2()
}
class "クラスC" {
}
"クラスA" -- "クラスB"
"クラスA" <|-- "クラスC"
....
```

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [6:37:52] 
$ gradle
Starting a Gradle Daemon (subsequent builds will be faster)

> Task :asciidoctor
asciidoctor: WARNING: image to embed not found or not readable: /Users/garbagetown/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit/src/docs/asciidoc/images/classediagram.png


BUILD SUCCESSFUL in 40s
2 actionable tasks: 2 executed

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/asciidoc-starter-kit on git:master x [6:42:11] 
$ tree build 
build
└── asciidoc
    ├── html5
    │   ├── asciidoctor.css
    │   ├── coderay-asciidoctor.css
    │   ├── images
    │   │   └── classediagram.png
    │   └── index.html
    └── pdf
        ├── asciidoctor.css
        ├── coderay-asciidoctor.css
        ├── images
        │   └── classediagram.png
        └── index.pdf

5 directories, 8 files
```

pdf に画像が入らない。。。

`images` attribute を絶対パスで記述すれば回避できる。本来は下記のように対応すべき？

- https://github.com/asciidoctor/asciidoctor-pdf/issues/271
- https://github.com/asciidoctor/asciidoctor-gradle-examples/issues/15


## links

- [図入りのAsciiDoc記述からPDFを生成する環境をGradleで簡単に用意する - Qiita](https://qiita.com/tokumoto/items/d37ab3de5bdbee307769)
	- "pdfの出力だけにすると画像の埋め込みができない。なので、html5生成を先に行い、そのときに生成される画像ファイルをpdf生成時に流用するというトリッキーな方法を用いている"
- [AsciiDoc と PlantUML と mermaid.js で素敵なテキストベース仕様書ライフ](https://ryuta46.com/112)