Syntax
=====

- [AsciiDoc Syntax Quick Reference | Asciidoctor](http://asciidoctor.org/docs/asciidoc-syntax-quick-reference/)

## Paragraphs

### Line breaks
- `+` で改行できる
- `[%hardbreaks]` でも改行できるけど使わない

### Literal
- 先頭一文字以上の空白で等幅フォントのブロックになる
- 記法揺れを避けるため `....` のブロックのほうがよい

### Admonition
- NOTE, TIP, IMPORTANT, WARNING, CAUTION
- admonition blocks を使ったほうがいい

### Lead paragraph
- あまり意識したことない...


## Formatted Text

### Bold, Italic, and Monospace
- markdown では bold と italic の区別がなかったはず
- `**` などふたつつなげることで前後にスペースを含めなくてよくなるが、` *` に統一したほうがいいと思う

### Marks and Custom Styling
- `#` で囲むとハイライト？
- `[.xxx]#xxxxx#` はごてごてしてて好みじゃない
- line-through は markdown の `~` のような記法がほしい

### Superscript and Subscript
- super は地味に使うし記法も直感的で好き

### Curved Quotation Marks and Apostrophes (Smart Quotes)
- ？正直あまりわかってないし不要っぽい

## Document Header
- `=` だけ。markdown の `=====` のようなオプションはない
- author と revision は使っていない

## Section Titles (Headings)
- デフォルトの article doctype では `=` のドキュメンとタイトル (Level 0) はひとつだけ
	- その他の文書タイプって何がある？
- Level5 (`<h6>`) まで

### Explicit id
- 明示しないとタイトル文言がそのまま id になる
	- prefix として `_` が付いたり
	- 重複してる場合は連番が付与されたり
	- ハイフンがアンダースコアに変わったり？など
- 思いがけない挙動をすることもあるので、Level 2 くらいまでは明示してもいいかも
- とは言え英語でタイトルを付けるのもなかなか難しい
	- **課題**

### Section anchors and links (Asciidoctor only)
- `setanchors` と `setlinks` を付ける

## Include Files
- これにより複数ファイルに分割できる

## Horizontal Rules and Page Breaks
### Horizontal ruleview result
- `'''`

### Page break
- `<<<` でページブレイクできる
	- HTML ではあまり意味がない？

## Lists
### Unordered
- `*` で統一したほうがいい？
	- markdown との互換性を取るなら `-` のほうがいい
- Level5 まで
- カスタマイズ可能

### Ordered
- `.`
	- markdown は `1. ` で書くべきだが、ついつい連番を振ってしまうので、asciidoc のほうが潔い
- Level5 まで
- カスタマイズ可能

### Checklist
- GFM との互換性を意識して `- [x]` がよい

### Labeled
- 記法揺れを防ぐために multi-line に統一する

### Q&A
- 使っていない


## Links

### External
- [Create Link - Chrome ウェブストア](https://chrome.google.com/webstore/detail/create-link/gcmghdmnkfdbncmnmlkkglmnnhagajbm?hl=ja) が便利

### Inter-document cross references (Asciidoctor only)
- <<document-b.adoc#section-b,Section B>> で統一


## Images
- `imagesdir` 属性を使って画像ディレクトリを相対パスで指定する
- image::sunset.jpg[Sunset,300,200] で alt とサイズ指定できる
- inline 画像は使っていない

## Videos
- 使っていない


## Source Code
- Code block with callouts
	- `// <1>` が便利


## More Delimited Blocks

### Sidebar
- 使っていない

### Admonition
- Open Block で統一したい

### Blockquote
- 引用元をいい感じに表示できる
- 技術文書で使うことは多くなさそう
- asciidoctor only な記法もあるが、覚えるのが大変なので asciidoc に統一でいいと思う

### Passthroughview
- 使っていない

### Openview
- いまいち理解していない...

### Custom substitutions
- いまいち理解していない...


## Block Id, Role and Options
- asciidoctor only な記法もあるが、覚えるのが大変なので asciidoc に統一でいいと思う
- Block Id は適宜付与したい
	- 内部相互参照時に悩まない
		- 自動付与だといろいろ罠がある
		- 同盟には連番がついたり、記号が置き換えられたり
	- Id の命名規約があると悩まなくてよいが、むずかしそう
- Role は使っていない
- Options はテーブルに必ず付与した


## Comments
- 行コメントは補足目的で使った
- ブロックコメントは弊害が多いので使わなかった
	- 念のためコメントアウトして残す -> git でよい
	- 過剰に装飾したヘッダ -> 本質的に無意味


## Tables
- markdown よりはるかに書きやすい
	- 最初は脳内変換できないがすぐになれる
- ヘッダ行も含め、列はすべて改行で統一した
- cols はトータル 100 になるように振った
	- `a` をつけると asciidoc 記法が使えるようになる
- options は header のみ
	- 他にも属性が多数ある
	- http://asciidoc.org/userguide.html#X69
- rowspan, colspan は直感的ではないが使った

### Table from CSV data
- 使っていないけど便利そう


## UI Macros
- 使っていない

## Attributes and Substitutions
- 知らなかったので使っていないけど便利そう


## Text Replacement
- 使っていないけど便利そう


## Escaping Text
- マークアップ前後にスペースを入れなかったために意図せずエスケープされていることが多かった
	-	Atom の Adoc Preview プラグインではマークアップされてレンダリングされるのでミスに気付かなかった
	- Guard で自動リビルドするとよいのかもしれない
		- が、Winodws に Guard を入れるのは大変そう
- 高島さんが多用していたインラインマークアップはパススルー？？？
	- Custom CSS classes だった。
	- css クラスを style で書いている。あまり行儀良くない...

## Table of Contents (ToC)
- すごく便利


## Bibliography
- 使っていないけど便利そう

## Footnotes
- 使っていないけど積極的に使っていきたい


## Markdown Compatibility
- asciidoctor only だが、markdown からは離れられないので許容していいと思う
	- title
	- faced code block
	- blockquote
	- horizontal line
	

## User Manual and Help
- あとはマニュアルをよく読む
