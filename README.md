JJUG CCC 2017 Fall
=====

## タイトル

AsciiDocとPlantUMLでドキュメント作成

## 概要

AsciiDocはMarkdownのような軽量マークアップ言語です。Markdownと同様に簡易な記法でありながらも表現力に優れ、複数ファイルで構成される本格的な技術ドキュメントを作成できます。HTMLやPDFに変換することも可能で、Spring Frameworkのドキュメントにも採用されています。

PlantUMLはクラス図やシーケンス図などのUMLをテキストベースで記述するDSL(ドメイン固有言語)で、PNGを出力できます。

いずれもAtomプラグインで出力結果をプレビューしながら執筆できますし、Maven/Gradleプラグインでビルドできます。また、テキストファイルなのでgitでの管理に向いています。

本セッションでは、AsciiDoc, PlantUML, Atom, Maven/Gradle, git, Jenkins を組み合わせて技術ドキュメントを作成する方法と、実際に採用して得られた知見を紹介します。


## TODO

- [ ] asciidoc 概要
	- 歴史や実装言語、事例など
	- [AsciiDoc入門 - Qiita](https://qiita.com/xmeta/items/de667a8b8a0f982e123a)
	- [Asciidoctor Documentation | Asciidoctor](http://asciidoctor.org/docs/)
- [ ] asciidoc 文法全網羅
	- 複数の文法がある要素を確認し、利点欠点を明らかにする
	- チームで作業する場合に採用する文法を明らかにする
- [ ] markdown との比較
	- markdown を使う機会は依然多く、文法の差異を把握することが大事
- [ ] spring framework ドキュメント環境確認
	- [spring-framework/CONTRIBUTING-DOCUMENTATION.adoc at master · spring-projects/spring-framework](https://github.com/spring-projects/spring-framework/blob/master/CONTRIBUTING-DOCUMENTATION.adoc)
	- [spring-framework/src/docs/asciidoc at master · spring-projects/spring-framework](https://github.com/spring-projects/spring-framework/tree/master/src/docs/asciidoc)
- [ ] plantuml 文法全網羅
	- シーケンス図以外でドキュメント作成に有用なものを明らかにする
	- ライセンスも再確認
- [ ] atom plugin インストール手順作成
	- ふつうに使いまわせる資料になるはず
	- [ ] [Graphviz \| Graphviz \- Graph Visualization Software](http://www.graphviz.org/)
	- [ ] [asciidoc\-preview](https://atom.io/packages/asciidoc-preview)
	- [ ] [language\-asciidoc](https://atom.io/packages/language-asciidoc)
	- [ ] [plantuml\-viewer](https://atom.io/packages/plantuml-viewer)
	- [ ] [language\-plantuml](https://atom.io/packages/language-plantuml)
- [ ] maven, gradle ビルド環境構築
	- maven でも一発で plantuml をビルドできる？
- [ ] jenkins ビルド環境構築
	- ビルドした html の表示に iframe を使わないプラグインを探す
- その他
	- [ ] [asciidoctor/asciidoclet: A Javadoc Doclet based on Asciidoctor that lets you write Javadoc in the AsciiDoc syntax\.](https://github.com/asciidoctor/asciidoclet)
	- [ ] [Abnaxos/markdown\-doclet: A Doclet that allows the use of Markdown in JavaDoc comments\.](https://github.com/Abnaxos/markdown-doclet)
	- [ ] [RedPen](http://redpen.cc/)


## 知見

- ReST はつらい
	- 文法が直感的でない
		- 具体的には？
	- エコシステムが充実していない
		- github プレビュー
		- atom ブラグインプレビュー
- vs markdown
	- pros
		- 改行
		- テーブル
		- bold, italic
		- NOTE や WARNING はかなり使う
		- cross reference
	- cons
		- header とブロックがいずれも `=====` でわかりづらい？
		- strikethrouge の文法がイケてない
- 文法がゆれる
	- インラインコードはバッククォートひとつ？ふたつ？
	- INFO や WARNING はブロック？
	- link: でリンク書いたり
	- リストはインデントする？
		- `-` と `*` どちらで始める？
- エディタ
	- Asciidoc FX はおすすめしない
	- Atom 最高