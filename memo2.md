spring framework
=====

spring のドキュメントも asciidoc で書かれている。

- https://github.com/spring-projects/spring-framework/tree/master/src/docs/asciidoc

## Guard

Guardfile があるので、[Guard](http://guardgem.org/) で変更を検知してリアルタイムでビルドしていると思われる。

カレントディレクトリ配下の `*.adoc` を監視しているっぽい

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall on git:master x [5:34:59] 
$ grep watch src/docs/asciidoc/Guardfile
  watch(/^.*\.adoc$/) {|m|
  watch(%r{build/.+\.(css|js|html)$})

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall on git:master x [5:35:02] 
$ ll src/docs/asciidoc | grep adoc
-rw-r--r--   1 garbagetown  staff   146K  9 22 06:54 appendix.adoc
-rw-r--r--   1 garbagetown  staff   1.3K  9 22 06:54 core.adoc
-rw-r--r--   1 garbagetown  staff   264K  9 22 06:54 data-access.adoc
-rw-r--r--   1 garbagetown  staff   1.9K  9 22 06:54 index.adoc
-rw-r--r--   1 garbagetown  staff   340K  9 22 06:54 integration.adoc
-rw-r--r--   1 garbagetown  staff    24K  9 22 06:54 kotlin.adoc
-rw-r--r--   1 garbagetown  staff   7.2K  9 22 06:54 overview.adoc
-rw-r--r--   1 garbagetown  staff   3.0K  9 22 06:54 reactive-web.adoc
-rw-r--r--   1 garbagetown  staff   207K  9 22 06:54 testing.adoc
-rw-r--r--   1 garbagetown  staff   789B  9 22 06:54 web.adoc
```

意外と Guard が入っていた。

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall on git:master o [5:28:53] 
$ ruby -v
ruby 2.2.2p95 (2015-04-13 revision 50295) [x86_64-darwin13]

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall on git:master o [6:57:59] 
$ gem -v
2.4.5

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall on git:master o [5:28:57] 
$ guard -v
Guard version 2.13.0
```

ので試してみる。

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall on git:master x [5:37:04] 
$ cd src/docs/asciidoc 

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:37:07] 
$ guard start
05:37:18 - ERROR - Invalid Guardfile, original error is: 
> [#] 
> [#] cannot load such file -- asciidoctor, 
> [#] backtrace: 
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/2.2.0/rubygems/core_ext/kernel_require.rb:121:in `require'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/2.2.0/rubygems/core_ext/kernel_require.rb:121:in `require'
> [#] 	(dsl)> ./Guardfile:1:in `evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:377:in `instance_eval'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:377:in `evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/guardfile/evaluator.rb:91:in `evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard.rb:134:in `_evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard.rb:49:in `setup'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/commander.rb:32:in `start'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/cli/environments/valid.rb:16:in `start_guard'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/cli.rb:122:in `start'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/command.rb:27:in `run'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/invocation.rb:126:in `invoke_command'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor.rb:359:in `dispatch'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/base.rb:440:in `start'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/aruba_adapter.rb:32:in `execute'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/aruba_adapter.rb:19:in `execute!'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/bin/_guard-core:11:in `<main>'
```

asciidoctor がない。そりゃそうだ。

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:37:18] C:1
$ gem install asciidoctor
Fetching: asciidoctor-1.5.6.1.gem (100%)
Successfully installed asciidoctor-1.5.6.1
Parsing documentation for asciidoctor-1.5.6.1
Installing ri documentation for asciidoctor-1.5.6.1
Done installing documentation for asciidoctor after 8 seconds
1 gem installed

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:38:51] 
$ asciidoctor -v
Asciidoctor 1.5.6.1 [http://asciidoctor.org]
Runtime Environment (ruby 2.2.2p95 (2015-04-13 revision 50295) [x86_64-darwin13]) (lc:UTF-8 fs:UTF-8 in:- ex:UTF-8)
```

もう一度。

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:39:14] 
$ guard start
05:39:45 - ERROR - Could not load 'guard/shell' or find class Guard::Shell
05:39:45 - ERROR - Error is: cannot load such file -- guard/shell
05:39:45 - ERROR - /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/2.2.0/rubygems/core_ext/kernel_require.rb:121:in `require'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/2.2.0/rubygems/core_ext/kernel_require.rb:121:in `require'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/plugin_util.rb:105:in `rescue in plugin_class'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/plugin_util.rb:97:in `plugin_class'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/plugin_util.rb:56:in `initialize_plugin'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/internals/plugins.rb:26:in `add'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:185:in `block in guard'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:182:in `each'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:182:in `guard'
> [#] /Users/garbagetown/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc/Guardfile:8:in `evaluate'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:377:in `instance_eval'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:377:in `evaluate'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/guardfile/evaluator.rb:91:in `evaluate'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard.rb:134:in `_evaluate'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard.rb:49:in `setup'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/commander.rb:32:in `start'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/cli/environments/valid.rb:16:in `start_guard'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/cli.rb:122:in `start'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/command.rb:27:in `run'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/invocation.rb:126:in `invoke_command'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor.rb:359:in `dispatch'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/base.rb:440:in `start'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/aruba_adapter.rb:32:in `execute'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/aruba_adapter.rb:19:in `execute!'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/bin/_guard-core:11:in `<main>'
05:39:45 - ERROR - Invalid Guardfile, original error is: 
> [#] 
> [#] Could not load class: "Shell", 
> [#] backtrace: 
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/plugin_util.rb:57:in `initialize_plugin'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/internals/plugins.rb:26:in `add'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:185:in `block in guard'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:182:in `each'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:182:in `guard'
> [#] 	(dsl)> ./Guardfile:8:in `evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:377:in `instance_eval'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:377:in `evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/guardfile/evaluator.rb:91:in `evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard.rb:134:in `_evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard.rb:49:in `setup'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/commander.rb:32:in `start'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/cli/environments/valid.rb:16:in `start_guard'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/cli.rb:122:in `start'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/command.rb:27:in `run'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/invocation.rb:126:in `invoke_command'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor.rb:359:in `dispatch'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/base.rb:440:in `start'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/aruba_adapter.rb:32:in `execute'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/aruba_adapter.rb:19:in `execute!'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/bin/_guard-core:11:in `<main>'
```

Guard::Shellがないらしい。なんだろう？guard-shell がない？

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:44:38] C:1
$ gem environment     
RubyGems Environment:
  - RUBYGEMS VERSION: 2.4.5
  - RUBY VERSION: 2.2.2 (2015-04-13 patchlevel 95) [x86_64-darwin13]
  - INSTALLATION DIRECTORY: /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0
  - RUBY EXECUTABLE: /Users/garbagetown/.rbenv/versions/2.2.2/bin/ruby
  - EXECUTABLE DIRECTORY: /Users/garbagetown/.rbenv/versions/2.2.2/bin
  - SPEC CACHE DIRECTORY: /Users/garbagetown/.gem/specs
  - SYSTEM CONFIGURATION DIRECTORY: /Users/garbagetown/.rbenv/versions/2.2.2/etc
  - RUBYGEMS PLATFORMS:
    - ruby
    - x86_64-darwin-13
  - GEM PATHS:
     - /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0
     - /Users/garbagetown/.gem/ruby/2.2.0
  - GEM CONFIGURATION:
     - :update_sources => true
     - :verbose => true
     - :backtrace => false
     - :bulk_threshold => 1000
  - REMOTE SOURCES:
     - https://rubygems.org/
  - SHELL PATH:
     - /Users/garbagetown/.rbenv/versions/2.2.2/bin
     - /usr/local/Cellar/rbenv/1.1.1/libexec
     - /Users/garbagetown/.sdkman/candidates/gradle/current/bin
     - /Users/garbagetown/perl5/bin
     - /Users/garbagetown/.rbenv/shims
     - /usr/local/bin
     - /usr/bin
     - /bin
     - /usr/sbin
     - /sbin
     - /usr/local/go/bin
     - /Library/Java/JavaVirtualMachines/jdk1.8.0_131.jdk/Contents/Home/bin
     - /usr/local/apache-maven-3.2.1/bin
     - /Users/garbagetown/dev/activator-1.3.10-minimal/bin
     - /Users/garbagetown/dev/playframework/play-1.4.2

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:45:38] 
$ ll /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-
guard-2.13.0/          guard-minitest-2.3.1/
```

とりあえず入れてみる。

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:45:38] 
$ gem search guard-shell                                                     

*** REMOTE GEMS ***

guard-shell (0.7.1)
guard-sheller (1.0.2)
guard-shellexec (1.0.0)

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:48:09] 
$ gem install guard-shell
Fetching: guard-compat-1.2.1.gem (100%)
Successfully installed guard-compat-1.2.1
Fetching: guard-shell-0.7.1.gem (100%)
Successfully installed guard-shell-0.7.1
Parsing documentation for guard-compat-1.2.1
Installing ri documentation for guard-compat-1.2.1
Parsing documentation for guard-shell-0.7.1
Installing ri documentation for guard-shell-0.7.1
Done installing documentation for guard-compat, guard-shell after 0 seconds
2 gems installed
```

今度は LiveReload がない。自動で依存関係を解決して引っ張ってくれないのかしら・・・

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:49:28] 
$ guard start
05:49:44 - ERROR - Could not load 'guard/livereload' or find class Guard::Livereload
05:49:44 - ERROR - Error is: cannot load such file -- guard/livereload
05:49:44 - ERROR - /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/2.2.0/rubygems/core_ext/kernel_require.rb:121:in `require'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/2.2.0/rubygems/core_ext/kernel_require.rb:121:in `require'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/plugin_util.rb:105:in `rescue in plugin_class'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/plugin_util.rb:97:in `plugin_class'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/plugin_util.rb:56:in `initialize_plugin'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/internals/plugins.rb:26:in `add'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:185:in `block in guard'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:182:in `each'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:182:in `guard'
> [#] /Users/garbagetown/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc/Guardfile:17:in `evaluate'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:377:in `instance_eval'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:377:in `evaluate'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/guardfile/evaluator.rb:91:in `evaluate'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard.rb:134:in `_evaluate'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard.rb:49:in `setup'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/commander.rb:32:in `start'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/cli/environments/valid.rb:16:in `start_guard'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/cli.rb:122:in `start'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/command.rb:27:in `run'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/invocation.rb:126:in `invoke_command'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor.rb:359:in `dispatch'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/base.rb:440:in `start'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/aruba_adapter.rb:32:in `execute'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/aruba_adapter.rb:19:in `execute!'
> [#] /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/bin/_guard-core:11:in `<main>'
05:49:44 - ERROR - Invalid Guardfile, original error is: 
> [#] 
> [#] Could not load class: "Livereload", 
> [#] backtrace: 
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/plugin_util.rb:57:in `initialize_plugin'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/internals/plugins.rb:26:in `add'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:185:in `block in guard'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:182:in `each'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:182:in `guard'
> [#] 	(dsl)> ./Guardfile:17:in `evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:377:in `instance_eval'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/dsl.rb:377:in `evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/guardfile/evaluator.rb:91:in `evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard.rb:134:in `_evaluate'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard.rb:49:in `setup'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/commander.rb:32:in `start'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/cli/environments/valid.rb:16:in `start_guard'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/cli.rb:122:in `start'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/command.rb:27:in `run'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/invocation.rb:126:in `invoke_command'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor.rb:359:in `dispatch'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/thor-0.19.1/lib/thor/base.rb:440:in `start'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/aruba_adapter.rb:32:in `execute'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/lib/guard/aruba_adapter.rb:19:in `execute!'
> [#] 	(dsl)> /Users/garbagetown/.rbenv/versions/2.2.2/lib/ruby/gems/2.2.0/gems/guard-2.13.0/bin/_guard-core:11:in `<main>'
```

インストールして再実行。

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:49:44] C:1
$ gem search livereload

*** REMOTE GEMS ***

guard-livereload (2.5.2)
inesita-livereload (0.1.0)
jekyll-livereload (0.2.2)
leifcr-rack-livereload (0.3.19)
livereload (1.6.1)
livereload_rails (1.0.2)
middleman-livereload (3.4.6)
rack-livereload (0.3.16)
react-rails-livereloadable_renderer (0.0.1)

# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:50:46] 
$ gem install guard-livereload
Fetching: http_parser.rb-0.6.0.gem (100%)
Building native extensions.  This could take a while...
Successfully installed http_parser.rb-0.6.0
Fetching: em-websocket-0.5.1.gem (100%)
Successfully installed em-websocket-0.5.1
Fetching: guard-livereload-2.5.2.gem (100%)
Successfully installed guard-livereload-2.5.2
Parsing documentation for http_parser.rb-0.6.0
Installing ri documentation for http_parser.rb-0.6.0
Parsing documentation for em-websocket-0.5.1
Installing ri documentation for em-websocket-0.5.1
Parsing documentation for guard-livereload-2.5.2
Installing ri documentation for guard-livereload-2.5.2
Done installing documentation for http_parser.rb, em-websocket, guard-livereload after 4 seconds
3 gems installed
```

Chrome に LiveReload をインストールして実行。

```sh
# garbagetown @ garbagetown in ~/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc on git:master x [5:55:10] 
$ guard start
05:55:16 - INFO - LiveReload is waiting for a browser to connect.
05:55:16 - INFO - Guard is now watching at '/Users/garbagetown/dev/repos/garbagetown/jjug-ccc-2017-fall/src/docs/asciidoc'
[1] guard(main)> 05:55:25 - INFO - Browser connected.
05:57:50 - INFO - Reloading browser: build/index.html
05:58:57 - INFO - Reloading browser: build/index.html
05:59:43 - INFO - Reloading browser: build/index.html
06:00:04 - INFO - Reloading browser: build/index.html
06:00:05 - INFO - Reloading browser: build/index.html
[1] guard(main)> 
```

Chrome は自動でリロードされなかったけど、adoc のビルドは自動で実行されたので、よしとする。
