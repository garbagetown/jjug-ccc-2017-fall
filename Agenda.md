Agenda
=====

## AsciiDoc
- AsciiDoc とは
- asciidoctor とは

### PlantUML
- PlantUML とは

### Atom
- Graphviz とは
- Graphviz のインストール
- Atom パッケージ
	- [asciidoc\-preview](https://atom.io/packages/asciidoc-preview)
	- [language\-asciidoc](https://atom.io/packages/language-asciidoc)
	- [plantuml\-viewer](https://atom.io/packages/plantuml-viewer)
	- [language\-plantuml](https://atom.io/packages/language-plantuml)

### Maven/Gradle
- Maven によるビルド
- Gradle によるビルド

### git
- GitLab と GitHub フロー

### Jenkins
- Jenkins2
- Jenkinsfile
	- workspace
	- S3

### misc.
- asciidoclet
- ~~markdowndoclet~~